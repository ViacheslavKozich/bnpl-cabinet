import * as I from 'apis/cabinetApi/bnpl_cabinet_api/models';


/**
 * Конвертированный response, возвращаемый методами apiCalls. Два типа:
 * - при fulfilled { `ok`: true, `data`: data, `error`: null }
 * - при rejected { `ok`: false, `data`: null, `error`: error }
 * @property `ok` статус запроса
 * @property `data` response.data запроса
 * @property `error` ошибка запроса 
 */
export interface IConvertedResponse<D = any> {
	/** `true` при fulfilled response, `false` при rejected response */
	ok:    boolean;
	data:  null | D;
	error: string;
};

/**
 * response.data при fulfilled запросе на получение массива сущностей
 * @property `results`     Массив запрашиваемых сущностей
 * @property `pageSize`    Количество записей на стрницу из запроса
 * @property `currentPage` Номер текущей страницы
 * @property `pageCount`   Общее количество страниц
 * @property `totalCount`  Общее количество записей
 */
export interface IGetAllRespData<T> {
	readonly results:     Array<T>;
	readonly currentPage: number;
	readonly pageCount:   number;
	readonly pageSize:    number;
	readonly totalCount:  number;
};

/**
 * params для метода getUsersAll
 * @property `page`     Номер страницы
 * @property `pageSize` Количество записей на страницу
 * @property `role`     Роль или роли пользователей (через запятую)
 * @property `email`    Электронная почта
 * @property `phone`    Телефон
 * @property `q`        Полный поиск
 */
export interface IGetUsersAllParams {
	page:     number;
	pageSize: number;
	role?:    Array<string>;
	email?:   string;
	phone?:   string;
	q?:       string;
}
