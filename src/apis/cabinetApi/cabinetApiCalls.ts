import * as swagger from 'apis/cabinetApi/bnpl_cabinet_api';
import { cabinetAxios } from './cabinetAxios';
// Types
import { AxiosInstance, AxiosError, AxiosResponse } from 'axios';
import * as I from 'apis/cabinetApi/bnpl_cabinet_api/models';
import * as EI from './extraTypes';


class CabinetApiCalls {
	readonly axiosInstance: AxiosInstance;
	readonly companyApi;
	readonly helpersApi;
	readonly identityApi;
	readonly profileApi;
	readonly userApi;

	constructor(axiosInstance: AxiosInstance) {
		this.axiosInstance = axiosInstance;
		this.companyApi = new swagger.CompanyApi(undefined, undefined, this.axiosInstance);
		this.helpersApi = new swagger.HelpersApi(undefined, undefined, this.axiosInstance);
		this.identityApi = new swagger.IdentityApi(undefined, undefined, this.axiosInstance);
		this.profileApi = new swagger.ProfileApi(undefined, undefined, this.axiosInstance);
		this.userApi = new swagger.UserApi(undefined, undefined, this.axiosInstance);
	}

	/**Метод преобразовывает fulfilled AxiosResponse для использования в приложении
	 * @param response AxiosResponse, возвращаемый api
	 * @returns преобразованный response
	 */
	private convertFulfilledResponse(response: AxiosResponse): EI.IConvertedResponse {
		const convertedResponse: EI.IConvertedResponse = {
			ok:    true,
			data:  response.data,
			error: ''
		};
		return convertedResponse;
	}

	/**Метод преобразовывает rejected AxiosResponse и подбирает текст ошибки
	 * @param error ошибка, пойманнаая при запросе на api
	 * @returns преобразованный response
	 */
	private convertRejectedResponse(error: any): EI.IConvertedResponse {
		let errorMessage: string;
		if (error.response) {
			const errResp = error.response;
			if (errResp.data?.code && errResp.data?.message) {
				errorMessage = `Ошибка ${errResp.data.code}: ${errResp.data.message}`;
			} else {
				errorMessage = `Ошибка ${errResp.status}: ${errResp.statusText}`;
			}
		} else if (error.request) {
			errorMessage = `Ошибка запроса на сервер`;
		} else {
			errorMessage = `Неизвестная ошибка в процессе выполнения запроса`;
		}

		const convertedResponse: EI.IConvertedResponse = {
			ok:    false,
			data:  null,
			error: errorMessage
		};
		return convertedResponse;
	}


	//! ========== Company API ==========
	/**[ POST ] /company                        | Регистрация организации в сервисе
	 * @param unp УНП компании
	 */
	async createCompany(unp: string): Promise<EI.IConvertedResponse<I.CompanyProfile>> {
		const requestBody = { unp };
		try {
			const createdCompany = await this.companyApi.companyPost(requestBody);
			return this.convertFulfilledResponse(createdCompany);
		} catch(error) {
			return this.convertRejectedResponse(error);
		}
	}

	/**[PATCH ] /company/markfordeletion/{id}   | Добавление компании пометки на удаление
	 * @param id идентификатор компании
	 */
	async markCompanyForDeletion(id: string): Promise<EI.IConvertedResponse<Date>> {
		try {
			const dateOfDeletion = await this.companyApi.companyMarkfordeletionIdPatch(id);
			return this.convertFulfilledResponse(dateOfDeletion)
		} catch(error) {
			return this.convertRejectedResponse(error);
		}
	}
	
	/**[PATCH ] /company/unmarkfordeletion/{id} | Снятие с компании пометки на удаление
	 * @param id идентификатор компании
	 */
	async unmarkCompanyForDeletion(id: string): Promise<EI.IConvertedResponse<Date>> {
		try {
			const data = await this.companyApi.companyUnmarkfordeletionIdPatch(id);
			return this.convertFulfilledResponse(data);
		} catch(error) {
			return this.convertRejectedResponse(error);
		}
	}

	/**[ GET  ] /company/{id}                   | Получение профиля компании
	 * @param id идентификатор компании
	 */
	async getCompanyById(id: string): Promise<EI.IConvertedResponse<I.CompanyProfile>> {
		try {
			const company = await this.companyApi.companyIdGet(id);
			return this.convertFulfilledResponse(company);
		} catch(error) {
			return this.convertRejectedResponse(error);
		}
	}

	/**[ PUT  ] /company/{id}                   | Обновление профиля компании
	 * @param id идентификатор компании
	 * @param companyProfile измененный профиль компании 
	 */
	async updateCompany(id: string, companyProfile: I.CompanyProfile): Promise<EI.IConvertedResponse<I.CompanyProfile>> {
		try {
			const updatedCompany = await this.companyApi.companyIdPut(id, companyProfile);
			return this.convertFulfilledResponse(updatedCompany);
		} catch(error) {
			return this.convertRejectedResponse(error);
		}
	}
	//! Не реализовано на сервере
	/**[ POST ] /company/contacts               | Добавление контакта к профилю компании
	 * @param user профиль пользователя, добавляемого как контакт компании
	 */
	async createCompanyContact(user: I.User): Promise<EI.IConvertedResponse<I.User>> {
		try {
			const createdCompanyContact = await this.companyApi.companyContactsPost(user);
			return this.convertFulfilledResponse(createdCompanyContact);
		} catch(error) {
			return this.convertRejectedResponse(error);
		}
	}
	//! Не реализовано на сервере
	/**[ PUT  ] /company/contacts/{contactId}   | Обновление контакта у профиля компании
	 * @param id идентификатор пользователя, являющегося контактом компании
	 * @param user обновленный профиль пользователя
	 */
	async updateCompanyContact(id: string, user: I.User): Promise<EI.IConvertedResponse<I.User>> {
		try {
			const updatedCompanyContact = await this.companyApi.companyContactsContactIdPut(id, user);
			return this.convertFulfilledResponse(updatedCompanyContact);
		} catch(error) {
			return this.convertRejectedResponse(error);
		}
	}
	//! Не реализовано на сервере
	/**[DELETE] /company/contacts/{contactId}   | Удаление контакта у профиля компании
	 * @param id идентификатор пользователя, являющегося контактом компании
	 */
	async deleteCompanyContact(id: string): Promise<EI.IConvertedResponse<I.User>> {
		try {
			const data = await this.companyApi.companyContactsContactIdDelete(id);
			return this.convertFulfilledResponse(data);
		} catch(error) {
			return this.convertRejectedResponse(error);
		}
	}


	//! ========== Identity API ==========
	// модуль в tokenService


	//! ========== Profile API ==========
	/**[ GET  ] /profile  | Получение профиля
	 */
	async getCurrentUserProfile(): Promise<EI.IConvertedResponse<I.User>> {
		try {
			const currentUserProfile = await this.profileApi.profileGet();
			return this.convertFulfilledResponse(currentUserProfile);
		} catch(error) {
			return this.convertRejectedResponse(error);
		}
	}

	/**[ PUT  ] /profile  | Обновление профиля
	 * @param user профиль текущего пользователя на обновление
	 */
	async updateCurrentUserProfile(user: I.User): Promise<EI.IConvertedResponse<I.User>> {
		try {
			const updatedCurrentUserProfile = await this.profileApi.profilePut(user);
			return this.convertFulfilledResponse(updatedCurrentUserProfile);
		} catch(error) {
			return this.convertRejectedResponse(error);
		}
	}


	//! ========== User API ==========
	/**[ GET  ] /user/{id} | Получение информации о пользователе
	 * @param id идентификатор пользователя
	 */
	async getUserById(id: string): Promise<EI.IConvertedResponse<I.User>> {
		try {
			const user = await this.userApi.userIdGet(id);
			return this.convertFulfilledResponse(user);
		} catch(error) {
			return this.convertRejectedResponse(error);
		}
	}

	/**[ PUT  ] /user/{id} | Обновление информации по пользователю
	 * @param id идентификатор пользователя
	 * @param user обновляемый пользователь
	 */
	async updateUser(id: string, user: I.User): Promise<EI.IConvertedResponse<I.User>> {
		try {
			const updatedUser = await this.userApi.userIdPut(id, user);
			return this.convertFulfilledResponse(updatedUser);
		} catch(error) {
			return this.convertRejectedResponse(error);
		}
	}

	/**[DELETE] /user/{id} | Удаление пользователя
	 * @param id идентификатор пользователя
	 */
	async deleteUserById(id: string): Promise<EI.IConvertedResponse<I.User>> {
		try {
			const data = await this.userApi.userIdDelete(id);
			return this.convertFulfilledResponse(data);
		} catch(error) {
			return this.convertRejectedResponse(error);
		}
	}

	/**[ GET  ] /user/{id} | Получение списка пользователей
	 * @param queryParams параметры поиска
	 */
	async getUsersAll(queryParams: EI.IGetUsersAllParams): Promise<EI.IConvertedResponse<I.UserPagedResult>> {
		try {
			const users = await this.userApi.userGet(
				queryParams.page,
				queryParams.pageSize,
				queryParams.role?.join(','),
				queryParams.email,
				queryParams.phone,
				queryParams.q
			);
			return this.convertFulfilledResponse(users);
		} catch (error) {
			return this.convertRejectedResponse(error);
		}
	}

	/**[ POST ] /user      | Добавление пользователя
	 * @param user создаваемый пользователь
	 */
	async createUser(user: I.User): Promise<EI.IConvertedResponse<I.User>> {
		try {
			const createdUser = await this.userApi.userPost(user);
			return this.convertFulfilledResponse(createdUser);
		} catch(error) {
			return this.convertRejectedResponse(error);
		}
	}

}

const cabinetApiCalls = new CabinetApiCalls(cabinetAxios.instance);

export { cabinetApiCalls };
