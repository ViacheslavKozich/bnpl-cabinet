import { AxiosFactory } from '../axiosFactory'
import { globalAppConfig } from 'appConfig';
import { tokensService } from 'app/auth/tokens/tokensService';


const cabinetAxios = new AxiosFactory({
	baseURL: globalAppConfig.appConfig.API_URL,
	getAccessToken: tokensService.getAccessTokenLocal,
	updateTokens: tokensService.updateTokensAfterAccessTokenExpEnd,
});

export { cabinetAxios };
