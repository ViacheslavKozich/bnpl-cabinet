//* Класс для создания axiosInstance с заданным baseURL и перехватчиками для headers.Authorization
//* new AxiosFactory().instance передается в конструкторы клиента swagger для api

import axios from 'axios';
// Types
import {
	AxiosResponse,
	AxiosInstance,
	AxiosRequestHeaders,
} from 'axios';


export interface IConvertedAxiosResponse<D = any, E = any> {
	/** `true` при fulfilled response, `false` при rejected response */
	ok:    boolean;
	data:  null | D;
	error: null | E;
};
export interface IAxiosFactoryParams {
	/** Дефолтный `baseURL` для axiosInstance */
	baseURL?: string;
	/** Дефолтные `headers` для axiosInstance */
	headers?: AxiosRequestHeaders;
	/** Ассинхронная функция, меняющая refreshToken на новые токены после 401 ошибки */
	updateTokens(): Promise<void>;
	/** Получение accessToken для установки в headers в запросaх к api через перехватчик  */
	getAccessToken(): string;
};

class AxiosFactory {
	/** экземпляр axios для запросов на api */
	readonly instance: AxiosInstance;
	updateTokens: IAxiosFactoryParams['updateTokens'];
	getAccessToken: IAxiosFactoryParams['getAccessToken'];

	constructor(initParams: IAxiosFactoryParams) {
		this.instance = axios.create({
			headers: {
				'Content-Type': 'application/json',
				...initParams.headers
			}
		});

		this.updateTokens = initParams.updateTokens;
		this.getAccessToken = initParams.getAccessToken;
		if (initParams.baseURL) {
			this.setBaseUrl(initParams.baseURL);
		}

		//* Перехватчик запроса
		this.instance.interceptors.request.use(
			// request fulfilled
			(config) => {
				const accessToken = this.getAccessToken();
				if (accessToken && config.headers) {
					config.headers['Authorization'] = `Bearer ${accessToken}`;
				}
				return config;
			},
			// request rejected
			(error) => {
				debugger
				Promise.reject(error);
			}
		);

		//* Перехватчик ответа
		this.instance.interceptors.response.use(
			// response fulfilled
			(value): AxiosResponse => {
				return value;
			},
			// response rejected
			async (error): Promise<AxiosResponse> => {
				const originalConfig = error.config;

				debugger
				//* Запрос завершен, сервер вернул response status code вне 2xx
				if (error.response) {
					console.log('error.response', error.response);
					// Если status==401, попытаться обновить токен и повторить запрос единожды
					if (error.response.status === 401 && !originalConfig._retry) {
						originalConfig._retry = true;
						try {
							await this.updateTokens();
							return this.instance(originalConfig);
						} catch(error) {
							debugger
							return Promise.reject(error);
						}
					}

					// Если все еще 401, значит что-то не так с пользователем
					if (error.response.status === 401 && originalConfig._retry === true) {
						console.log('повторно 401 ошибка');
						debugger
						// Отправить на auth/login?
					}

				//* Запрос совершен, но ответ не получен
				} else if(error.request) {
					// error.request — это экземпляр класса XMLHttpRequest в браузере
					console.log('error.request', error.request);

				//* Иначе запрос не совершен, ошибка при настройке запроса
				} else {
					console.log('error.message', error.message, error);
				}

				return Promise.reject(error);
			}
		);
	}

	/** 
	 * @method `setBaseUrl` задает динамический baseURL для instance
	 */
	setBaseUrl(baseURL: string): void {
		this.instance.defaults.baseURL = baseURL;
	}

}

export { AxiosFactory };
