interface DynamicAppConfig {
	environment: "DEV" | "TST" | "PROD",
	API_URL:     string,
	AUTH_URL:    string,
}

const defaultAppConfig: DynamicAppConfig = {
	environment: "DEV",
	API_URL:     "https://api-dev.hgrosh.by",
	AUTH_URL:    "https://ids.iii.by"
}

class GlobalAppConfig {
	appConfig: DynamicAppConfig = defaultAppConfig;
}

export const globalAppConfig = new GlobalAppConfig();

export const globalAppConfigUrl = 'appConfig.json';
