import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { sessionStorageHelper } from 'utils';


//* reducer для состояний, не нужно сохранять между сессиями
interface IAppInitialState {
	isLoading:   boolean;
	lastError:   string;
	accessToken: string;
}
const appInitialState: IAppInitialState = {
	isLoading:   false,	// Статус загрузки
	lastError:   '',    // Текст ошибки, пойманной при совершении запроса
	accessToken: sessionStorageHelper.getAccessToken(), // Токен из session storage
};
const appSlice = createSlice({
	name: 'app',
	initialState: appInitialState,
	reducers: {
		startCall: (state) => {
			state.isLoading = true;
			state.lastError = '';
		},
		endCall: (state) => {
			state.isLoading = false;
			state.lastError = '';
		},
		catchError: (state, action: PayloadAction<string>) => {
			state.isLoading = false;
			state.lastError = action.payload;
		},

		accessTokenFetched: (state, action: PayloadAction<string>) => {
			state.isLoading = false;
			state.accessToken = action.payload;
		},

		accessTokenUpdated: (state, action: PayloadAction<string>) => {
			state.accessToken = action.payload;
		},

		removeAccessTokenFromState: (state) => {
			state.accessToken = '';
		},

	}
});

export { appSlice };
