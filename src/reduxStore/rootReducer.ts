import { combineReducers } from 'redux';

import { authSlice } from 'app/auth/_redux/authSlice';
import { appSlice } from './appSlice';
import { profileSlice } from 'app/modules/profile/_redux/profileSlice';
import { companySlice } from 'app/modules/company/_redux/companySlice';


const rootReducer = combineReducers({
	// persisted reducer, сохраняется в local storage
	auth:    authSlice.reducer,
	// остальные редюсеры
	app:     appSlice.reducer,
	profile: profileSlice.reducer,
	company: companySlice.reducer,
});

export { rootReducer };
