import { useMemo, useCallback } from 'react';
import { useAppDispatch, useAppSelector, shallowEqual } from 'hooks';
import { useAuthContext } from 'app/providers';
import { identityApiCalls } from 'apis/identityServer/identityApiCalls';
import { tokensService } from 'app/auth/tokens/tokensService';
import { decodeTokenClaims } from 'utils';
// Types
import { MutableRefObject } from 'react';
import { User } from 'apis/cabinetApi/bnpl_cabinet_api/models';


/**
 * @summary Авторизованный пользователь и действия авторизации
 * @property `user` Профиль авторизованного через iii пользователя, полученный с cabinet api
 * @property `accessToken` accessToken пользователя
 * @property `isLoading` Статус обработки ассинхронного действия (профиль, токены)
 * @property `logInUser` Редирект пользователя на iii для авторизации и получения authCode
 * @property `setUserAuth` Обмен authCode на токены через cabinet api, получение профиля пользователя, запись в стор
 * @property `logOutUser` Удаление токенов и редирект на logout url iii
*/
interface IUseAuthReturn {
	/** Профиль авторизованного через iii пользователя, полученный с cabinet api */
	readonly user: User | null;
	/** При false доступ только к /auth, при true доступ к приложению */
	readonly isUserAuth: boolean;
	/** false - компания не привязана, во вкладке компании предлагается привязать */
	readonly isAdmin: boolean; 
	/** accessToken пользователя из app.reducer */
	readonly accessToken: string;
	/** refreshToken пользователя из app.reducer */
	readonly refreshToken: string;
	/** Статус обработки ассинхронного действия (профиль, токены) */
	readonly isLoading: boolean;
	/** Адрес редиректа после прохождения авторизации из auth context */
	readonly postAuthUrl: MutableRefObject<string>;
	/** Редирект пользователя на iii для авторизации и получения authCode */
	readonly logInUser: (fromUrl: string) => void;
	/** Обмен authCode на токены через cabinet api, получение профиля пользователя, запись в стор */
	readonly setUserAuth: (authCode: string) => Promise<void>;
	/** Удаление токенов и редирект на logout url iii */
	readonly logOutUser: VoidFunction;
};

const useAuth = (): IUseAuthReturn => {
	const { user, accessToken, refreshToken, idToken, isLoading } = useAppSelector((state) => {
		return { ...state.auth, ...state.app };
	}, shallowEqual);

	const isAdmin = useMemo(() => {
		return Boolean(decodeTokenClaims(accessToken)?.roles.includes('bnpl.admin'));
	}, [accessToken]);

	const { isUserAuth, postAuthUrl } = useAuthContext();

	const logInUser = useCallback((fromUrl: string): void => {
		// Редирект на iii для авторизации. fromUrl передадим в params.state для получения в хэше
		identityApiCalls.redirectToAuthServerForLogin(fromUrl);
	}, []);

	const setUserAuth = useCallback(async (authCode: string): Promise<void> => {
		// Получить на api.identity токены, задать токены в axiosInstance, state, session storage
		await tokensService.authorizeUserInApp(authCode);
	}, []);

	const logOutUser = useCallback((): void => {
		// Удалить токены из local и session storage
		tokensService.removeTokens();
		// Редирект на iii для логаута
		identityApiCalls.redirectToAuthServerForLogout(idToken);
	}, []);

	return {
		user,
		isUserAuth,
		isAdmin,
		accessToken,
		refreshToken,
		isLoading,
		postAuthUrl,
		logInUser,
		setUserAuth,
		logOutUser,
	};
}

export { useAuth };
