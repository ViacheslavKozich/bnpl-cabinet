import React from 'react';
import { Button, ButtonProps } from '@mui/material';


const AppButton: React.FC<ButtonProps> = ({ children, ...props }) => {
	return (
		<Button
			sx={{ textTransform: 'initial', ...props?.sx }}
			{...props}
		>
			{children}
		</Button>
	);
}

export { AppButton };
