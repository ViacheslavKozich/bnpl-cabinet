import React from 'react';
import { useTranslation } from 'react-i18next';
import {
	Card,
	CardActions,
	CardContent,
	CardHeader,
} from '@mui/material';
import { alpha } from '@mui/material/styles';

import {
	AppButton,
} from 'app/components';


interface IEditableCardProps {
	title:        string;
	isEditing:    boolean;
	setEditing:   (isEditing: boolean) => void;
	submitForm:   () => void;
	saveDisabled: boolean;
};
const EditableCard: React.FC<IEditableCardProps> = (props) => {
	const { children, title, isEditing, setEditing, submitForm, saveDisabled } = props;
	const { t } = useTranslation();
	
	const setEditingTrue =  (): void => setEditing(true);
	const setEditingFalse = (): void => setEditing(false);

	return (
		<Card>
			<CardHeader
				title={title}
				action={(!isEditing) && (<CardButton onClick={setEditingTrue} text={t('btn.edit')}/>)}
				sx={{ background: (theme) => alpha(theme.palette.primary.light, 0.2) }}
			/>

			<CardContent>
				{children}
			</CardContent>

			{(isEditing) && (
				<CardActions sx={{ background: (theme) => alpha(theme.palette.primary.light, 0.2), justifyContent: "flex-end" }}>
					<CardButton onClick={setEditingFalse} text={t('btn.cancel')}/>
					<SaveDataButton onClick={submitForm} disabled={saveDisabled} />
				</CardActions>
			)}
		</Card>
	);
}


interface ICardButtonProps {
	onClick: () => void;
	text:    string;
};
const CardButton: React.FC<ICardButtonProps> = ({ onClick, text }) => {
	return (
		<AppButton
			onClick={onClick}
		>
			{text}
		</AppButton>
	);
}


interface ISaveDataButtonProps {
	onClick:   () => void;
	disabled?: boolean;
};
const SaveDataButton: React.FC<ISaveDataButtonProps> = ({ onClick, disabled }) => {
	const { t } = useTranslation();

	return (
		<AppButton
			onClick={onClick}
			variant="outlined"
			disabled={disabled}
		>
			{t('btn.save')}
		</AppButton>
	);
}

export { EditableCard };
