import React from 'react';
import {
	CircularProgress,
	Typography,
} from '@mui/material';


const SplashScreen: React.FC = () => {
	return (
		<div id="splash-screen">
			<Typography
				variant="h5"
				color="primary"
				mb={2}
				sx={{ fontWeight: 500 }}
			>
				{'Добро пожаловать в сервис "BNPL by ESaS"'}
			</Typography>

			<CircularProgress size={60} />

			<Typography
				variant="h4"
				color="primary"
				mt={2}
				sx={{ fontWeight: 500 }}
			>
				{'Загрузка...'}
			</Typography>

		</div>
	);
}

export { SplashScreen };
