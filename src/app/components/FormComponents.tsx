import React, { useMemo } from 'react';
import { Formik, Form, Field } from 'formik';
import {
	TextField, TextFieldProps,
} from '@mui/material';
// Types
import { FormikErrors, FormikTouched } from 'formik';


interface TExtraTextFieldProps<T = any> {
	name: string;
	setFieldValue: (field: string, value: any, shouldValidate?: boolean) => void;
	touched: FormikTouched<T>;
	errors: FormikErrors<T>;
};
type TAppTextFieldProps = TextFieldProps & TExtraTextFieldProps;
const AppTextField: React.FC<TAppTextFieldProps> = ({ name, value, setFieldValue, touched, errors, ...props }) => {
	const isValueValid = touched[name] && Boolean(errors[name]);
	const visibleErrorMessage = touched[name] && errors[name];

	return (
		<Field as={TextField}
			name={name}
			error={isValueValid}
			helperText={visibleErrorMessage}
			inputProps={{ // При ховере не всплывает дефолтная браузерная подсказка
				required: false
			}}

			variant="outlined"
			margin="none"
			size="small"
			fullWidth={true}

			value={value || ''}
			onChange={(event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
				setFieldValue(name, event.target.value);
			}}

			{...props}
		/>
	);
}


export { AppTextField };
