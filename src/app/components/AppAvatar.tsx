import React from 'react';
import { Avatar, AvatarProps } from '@mui/material';


const AppAvatar: React.FC<AvatarProps> = ({ ...props }) => {
	
	return (
		<Avatar
			sx={{ width: 100, height: 100, ...props?.sx }}
			{...props}
		/>
	);
}

export { AppAvatar };
