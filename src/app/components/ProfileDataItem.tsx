import React, { useRef, useState, useLayoutEffect, ReactNode } from 'react';
import { Divider } from '@mui/material';
import { Grid, Typography } from 'app/components';


interface IProfileDataItemProps {
	title:     string;
	value?:    string | number | boolean | null;
	field?:    ReactNode;
	required?: boolean;
};
const ProfileDataItem: React.FC<IProfileDataItemProps> = ({ title, value, field, required }) => {
	const safeValue = value || '';

	const $divider = useRef<HTMLHRElement>(null);
	const [isLastChild, setLastChild] = useState(false);
	useLayoutEffect(() => {
		if ($divider.current && $divider.current.nextElementSibling === null) {
			setLastChild(true);
		}
	}, [$divider.current]);

	return (<>
		<Grid
			item
			container
			wrap="nowrap"
			justifyContent="space-between"
			alignItems="center"
			spacing={2}
		>
			<Grid item minWidth={200}>
				<Typography color="text.secondary">
					{title}
					{(field && required) && (
						<Typography component="span" color="error.main">{'\n*'}</Typography>
					)}
				</Typography>
			</Grid>

			<Grid item flexGrow={1}>
				{(field) || (
					<Typography>{safeValue}</Typography>
				)}
			</Grid>
		</Grid>

		<Divider
			ref={$divider}
			sx={{
				transform: (theme) => `translateY(${theme.spacing(1)})`,
				display: (isLastChild) ? 'none' : 'block',
			}}
		/>
	</>);
}

export { ProfileDataItem };
