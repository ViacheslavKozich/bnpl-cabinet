import React from 'react';
import { motion } from 'framer-motion';
import {
	Box,
	CircularProgress,
	Typography,
} from '@mui/material';


const PageContainer: React.FC = ({ children }) => {
	return (
		<motion.div
			animate={{ opacity: 1, y: 0 }}
			initial={{ opacity: 0, y: 10 }}
			exit={{ opacity: 0, y: -10 }}
			transition={{ duration: 0.5 }}
		>
			{children}
		</motion.div>
	);
}

interface IPageTitleProps {
	title: string;
	subtitle?: string;
}
const PageTitle: React.FC<IPageTitleProps> = ({ title, subtitle }) => {
	return (
		<Box sx={{ p: 2 }}>
			<Typography component="h2" variant="h5">{title}</Typography>
			<Typography variant="body2" color="text.secondary">{subtitle}</Typography>
		</Box>
	);
}

interface IPageLoaderContainerProps {
	isLoading: boolean;
}
const PageLoaderContainer: React.FC<IPageLoaderContainerProps> = ({ isLoading, children }) => {
	return (
		<Box sx={{ position: 'relative' }}>
			{(isLoading) && (
				<Box
					sx={{
						position: 'absolute',
						top: -3, left: -3, right: -3, bottom: -3,
						zIndex: 1000,
						display: 'flex',
						justifyContent: 'center',
						alignItems: 'center',
						backdropFilter: 'blur(3px)'
					}}
				>
					<CircularProgress color="primary" size={50} />
				</Box>
			)}

			{children}
		</Box>
	);
}


export { PageContainer, PageTitle, PageLoaderContainer };
