import React, { useRef } from 'react';
import { Routes, Route, Navigate } from 'react-router-dom';

import { Layout } from 'app/layout/Layout';
import { Auth } from 'app/auth/Auth';
import { AuthCodePage } from 'app/auth/pages/AuthCodePage';
import { LoginUserPage } from 'app/auth/pages/LoginUserPage';
import { RequiredAccessTokenHOC } from 'app/auth/pages/RequiredAccessTokenHOC';


const HomePage = React.lazy(() =>	import('app/modules/HomePage'));
const CompanyPage = React.lazy(() => import('app/modules/company/CompanyPage'));
const Profile = React.lazy(() => import('app/modules/profile/ProfilePage'));


const Router: React.FC = () => {
	return (
		<Routes>
			<Route path="/auth" element={<Auth/>}>
				<Route index element={<AuthCodePage/>} />
				<Route path="login" element={<LoginUserPage/>} />
			</Route>

			<Route path="/" element={
				// Если нету токена - редирект на auth/login 
				<RequiredAccessTokenHOC>
					<Layout/>
				</RequiredAccessTokenHOC>
			}>
				{/* <Route index element={<HomePage/>} /> */}
				<Route index element={<Navigate to="profile" replace/>} />
				<Route path="profile" element={<Profile/>} />
				<Route path="company" element={<CompanyPage/>} />
				<Route path="users"   element={<div>Users</div>} />
			</Route>

			<Route path="*" element={<h1>404 not found</h1>} />
		</Routes>
	);
}

export { Router };
