import React from 'react'
import { Outlet, Navigate } from 'react-router-dom';

import { useAuth } from 'hooks';
import { SplashScreenFallback } from 'app/providers';
// Types


//* location.pathname === '/auth*' && useAuth().accessToken === '' 
const Auth: React.FC = () => {
	const { isUserAuth, postAuthUrl } = useAuth();

	/**
	 * Текущий pathname === '/auth*'
	 * - пользователь не авторизован - нет accessToken:
	 *   - редирект RequiredAccessTokenHOC на /auth/login => редирект на iii для авторизации
	 * - пользователь авторизован - есть accessToken:
	 *   - редирект iii на /auth#code => получение и запись токенов, редирект на fromUrl
	 *   - переход на /auth => редирект на /
	 */
	return (isUserAuth) ? (
		<Navigate to={postAuthUrl.current} replace />
	) : (
		<>
			<SplashScreenFallback />
			<Outlet />
		</>
	);
}

export { Auth };
