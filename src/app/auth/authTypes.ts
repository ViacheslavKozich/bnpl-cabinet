import { MutableRefObject } from 'react';

/**
 * @summary Данные пользователя из профиля iii
 */
export interface IUser {
	sub: string;
	email: string;
	role: string;
	rights: Array<string>;
	name?: string;
	preferred_username?: string;
	family_name?: string;
	given_name?: string;
};

/**
 * @summary redirect url после авторизации на iii
 */
export interface IPostAuthUrl {
	postAuthUrl: MutableRefObject<string>
};
