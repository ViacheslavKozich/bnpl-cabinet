import React, { useEffect } from 'react';
import { useLocation } from 'react-router-dom';

import { useAuth } from 'hooks';


//* Попадаем только после редирект неавторизированного пользователя
//* с RequiredAccessTokenHOC для перенаправления на логин iii
const LoginUserPage: React.FC = () => {
	const { logInUser } = useAuth();

	// Стейт location после редиректа с RequiredAccessTokenHOC
	const location = useLocation();
	const fromUrl = location.state as string || '/';

	useEffect(() => {
		// debugger
		logInUser(fromUrl);
	}, []);

	return null;
}

export { LoginUserPage };
