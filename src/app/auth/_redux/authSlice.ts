import { createSlice, PayloadAction } from '@reduxjs/toolkit';
// Types
import { TokenResponse } from 'apis/cabinetApi/bnpl_cabinet_api/models';
import { IUser } from '../authTypes';


//* persisted reducer, сохраняется между сессиями в local storage. accessToken в session storage
interface IAuthInitialState {
	user:         IUser | null;
	// prevUsers: Array<IUser>;
	refreshToken: string;
	idToken:      string;
}
const authInitialState: IAuthInitialState = {
	user:         null,  // Текущий пользователь
	// prevUsers: [],    // Пользователи, авторизировавшиеся в этом браузере (из local storage)
	refreshToken: '', // Токены, получаемые с /identity
	idToken:      '',
};
const authSlice = createSlice({
	name: 'auth',
	initialState: authInitialState,
	reducers: {
		//* getTokensWithAuthCode / getTokensWithRefreshToken {status: ok}
		tokensFetched: (state, action: PayloadAction<TokenResponse>) => {
			const { refreshToken, idToken } = action.payload;
			state.refreshToken = refreshToken;
			state.idToken = idToken;
		},
		removeTokensFromState: (state) => {
			state.refreshToken = '';
			state.idToken = '';
		},

		//* getUserData {status: ok}
		userProfileFetched: (state, action: PayloadAction<IUser>) => {
			const fetchedUser = action.payload;
			state.user = fetchedUser;
			// удалить из массива prevUsers полученный профиль, т.к. мог изменится
			// state.prevUsers = state.prevUsers.filter((user) => user.sub !== fetchedUser.sub);
			// добавить в конец prevUsers полученный профиль
			// state.prevUsers.push(fetchedUser);
		},
		removeUserFromState: (state) => {
			state.user = null;
		},

	}
});

export { authSlice };
