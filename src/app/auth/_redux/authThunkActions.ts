import { appSlice } from 'reduxStore/appSlice';
import { authSlice } from './authSlice';
// Types
import { TThunkAction } from 'reduxStore/redux-store';
import { TokenResponse } from 'apis/cabinetApi/bnpl_cabinet_api/models';


const appActions = appSlice.actions;
const authActions = authSlice.actions;


export const dispatchTokensToState = (tokens: TokenResponse): TThunkAction<void> => (dispatch) => {
	const { accessToken, refreshToken, idToken } = tokens;
	// Задиспатчить refreshToken и idToken в persisted auth.reducer
	dispatch(authActions.tokensFetched({ refreshToken, idToken }));
	// Задиспатчить accessToken в app.reducer
	dispatch(appActions.accessTokenUpdated(accessToken));
}

export const removeUserAndTokensFromState = (): TThunkAction<void> => async (dispatch, getState) => {
	dispatch(authActions.removeUserFromState());
	dispatch(authActions.removeTokensFromState());
	//! Если удалять accessToken из стейта, не отрабатывает редирект
	// dispatch(appActions.removeAccessTokenFromState());
}
