import React from 'react';
import { Container, ContainerProps } from '@mui/material';


const AppContainer: React.FC<ContainerProps> = ({ children, ...props  }) => {
	return (
		<Container 
			maxWidth="md"
			sx={{
				px: 3,
				py: 1,
			}}
			{...props}
		>
			{children}
		</Container>
	);
}

export { AppContainer };
