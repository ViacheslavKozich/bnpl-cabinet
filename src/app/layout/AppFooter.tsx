import React, { useState, useMemo } from 'react';
import { useTranslation } from 'react-i18next';

import { 
	AppBar,
	Grid,
	Menu,     MenuProps,
	MenuItem,
} from '@mui/material';
import LanguageRoundedIcon from '@mui/icons-material/LanguageRounded';

import { AppContainer } from './AppContainer';
import { AppButton } from 'app/components';


const langs: any = {
	ru: 'Русский',
	en: 'English',
};

const footerBtns = [
	{ t: 'footer.reference'       },
	{ t: 'footer.confidentiality' },
	{ t: 'footer.terms'           },
];

const AppFooter: React.FC = () => {
	const { t, i18n } = useTranslation();

	// const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
	// const isMenuOpen = Boolean(anchorEl);
	// const openLangMenu = (event: React.MouseEvent<HTMLElement>) => {
	// 	setAnchorEl(event.currentTarget);
	// }
	// const closeLangMenu = () => {
	// 	setAnchorEl(null);
	// }

	let currentlang = i18n.language;
	let notCurrentLang = (currentlang === 'ru') ? 'en' : 'ru';
	const toggleLang = (event: React.MouseEvent<HTMLElement>) => {
		i18n.changeLanguage(notCurrentLang);
		// openLangMenu(event)
	}

	return (
		<AppBar
			component="footer"
			sx={{
				top: 'auto',
				bottom: 0,
			}}
		>
			<AppContainer>
				<Grid container justifyContent="space-between" alignItems="center">
					<AppButton
						onClick={(e) => toggleLang(e)}
						sx={{ textTransform: 'initial' }}
					>
						<LanguageRoundedIcon sx={{ mr: 1 }}/>
						{langs[currentlang]}
					</AppButton>
					{/* <LanguageMenu open={isMenuOpen} anchorEl={anchorEl} onClose={closeLangMenu}/> */}

					<Grid item>
						{footerBtns.map((item) => (
							<AppButton key={item.t}>{t(item.t)}</AppButton>
						))}
					</Grid>
				</Grid>
			</AppContainer>
		</AppBar>
	);
}

// const LanguageMenu: React.FC<MenuProps> = ({ open, anchorEl, onClose }) => {
// 	const { t, i18n } = useTranslation();

// 	return (
// 		<Menu
// 			open={open}
// 			onClose={onClose}
// 			anchorEl={anchorEl}
// 			anchorOrigin={{
// 				vertical: 'top',
// 				horizontal: 'left',
// 			}}
// 			transformOrigin={{
// 				vertical: 'top',
// 				horizontal: 'left',
// 			}}
// 		>
// 			<MenuItem>{langs[i18n.language]}</MenuItem>
// 			<MenuItem>{langs[i18n.language]}</MenuItem>
// 		</Menu>
// 	);
// }

export { AppFooter };
