import React from 'react'
import { NavLink } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import AccountBalanceRoundedIcon from '@mui/icons-material/AccountBalanceRounded';
import AccountCircleRoundedIcon from '@mui/icons-material/AccountCircleRounded';

import {
	List,
	ListItem,
	ListItemButton,
	ListItemIcon,
	ListItemText,
	ListSubheader,
} from '@mui/material';



interface IMainMenuListItem {
	to: string;
	t:  string;
	icon?: any;
};
type TMainMenuItemsList = Array<IMainMenuListItem>;

const mainMenuItemsList: TMainMenuItemsList = [
	// { to: '/',        t: 'navigation.home'    },
	{ to: '/company', t: 'navigation.company', icon: <AccountBalanceRoundedIcon/> },
	// { to: '/users',   t: 'navigation.users'   },
	{ to: '/profile', t: 'navigation.profile', icon: <AccountCircleRoundedIcon/>  },
];

const AppMainMenu: React.FC = () => {
	const { t } = useTranslation();

	return (<>
		<List dense>
			{mainMenuItemsList.map((item) => (
				<ListItem key={item.t} disableGutters>
					<NavLink to={item.to} style={{ width: '100%' }}>
						<ListItemButton sx={{ borderRadius: '4px' }}>
							<ListItemIcon sx={{ minWidth: 40, '& svg': {fill: (theme) => theme.palette.primary.main} }}>{item.icon}</ListItemIcon>
							<ListItemText
								primary={t(item.t)}
								sx={{ color: (theme) => theme.palette.primary.main }}
							/>
						</ListItemButton>
					</NavLink>
				</ListItem>
			))}
		</List>
	</>);
}

export { AppMainMenu };
