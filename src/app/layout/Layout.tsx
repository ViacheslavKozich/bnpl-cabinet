import { Outlet } from 'react-router-dom';
import { Grid } from '@mui/material'; 

import { AppHeader } from './AppHeader';
import { AppFooter } from './AppFooter';
import { AppContainer } from './AppContainer';
import { AppMainMenu } from './AppMainMenu';


const Layout: React.FC = () => {

	return (<>
		<AppHeader/>
		
		<AppContainer>
			<Grid
				container wrap="nowrap" justifyContent="space-between"
				sx={{ mb: 8 }}
			>
				<Grid item minWidth="200px" sx={{ pr: 2 }}>
					<AppMainMenu/>
				</Grid>

				<Grid item flexGrow={1} >
					<Outlet/>
				</Grid>
			</Grid>
		</AppContainer>
		
		<AppFooter/>
	</>)
}

export { Layout };
