import React from 'react';
import { Link } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { useAuth, useProfileCRUD } from 'hooks';


import {
	AppBar,
	Grid,
	Typography,
} from '@mui/material';

import { AppContainer } from './AppContainer';
import { AppButton } from 'app/components';


const AppHeader: React.FC = () => {
	const { logOutUser } = useAuth();
	const { userProfile } = useProfileCRUD();
	const { t } = useTranslation();

	return (
		<AppBar position="sticky">
			<AppContainer>
				<Grid container wrap="nowrap" justifyContent="space-between" alignItems="center">
					<Grid item>
						<Link to="/">
							<Typography color="primary" variant="h5">BNPL by ESaS</Typography>
						</Link>
					</Grid>
					
					<Grid item>
						<Typography
							component="span"
							sx={{ mr: 2 }}
						>
							{userProfile.email}
						</Typography>

						<AppButton
							variant="outlined"
							size="small"
							onClick={logOutUser}
						>
							Выйти
						</AppButton>
					</Grid>
				</Grid>
			</AppContainer>
		</AppBar>
	);
}

export { AppHeader };
