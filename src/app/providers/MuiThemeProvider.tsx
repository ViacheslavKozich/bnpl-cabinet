import React from 'react';
import { ThemeProvider } from '@mui/material/styles';
import { muiMainTheme } from 'app/theme/muiMainTheme';


const MuiThemeProvider: React.FC = ({ children }) => {
	return (
		<ThemeProvider theme={muiMainTheme}>
			{children}
		</ThemeProvider>
	);
}

export { MuiThemeProvider };
