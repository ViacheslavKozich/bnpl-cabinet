import React, { createContext, useContext, useState, useLayoutEffect, useRef } from 'react';
import { useAppSelector, shallowEqual, useProfileCRUD, useCompanyCRUD } from 'hooks';
// Types


interface IAuthContext {
	isUserAuth:  boolean;
	postAuthUrl: React.MutableRefObject<string>;
};
// @ts-ignores
const AuthContext = createContext<IAuthContext>();
const useAuthContext = () => useContext(AuthContext);

const AuthProvider: React.FC = ({ children }) => {
	const postAuthUrl = useRef<string>('/');

	//* isUserAuth для перевода приложения между состояниями
	const { accessToken } = useAppSelector((state) => state.app, shallowEqual);
	const [isUserAuth, setUserAuth] = useState<boolean>(Boolean(accessToken));
	useLayoutEffect(() => {
		if (accessToken && !isUserAuth) {
			setUserAuth(true);
		}
	}, [accessToken]);

	//* После получения токенов загружаем профиль и компанию
	const { fetchCurrentUserProfile, userProfile } = useProfileCRUD();
	const { fetchCompany } = useCompanyCRUD();
	useLayoutEffect(() => {
		if (accessToken) {
			fetchCurrentUserProfile();
		}
	}, [accessToken]);

	useLayoutEffect(() => {
		if (userProfile.companyId) {
			fetchCompany(userProfile.companyId);
		}
	}, [userProfile.companyId]);

	const value = {
		isUserAuth,
		postAuthUrl,
	};

	return (
		<AuthContext.Provider value={value}>
			{children}
		</AuthContext.Provider>
	);
}

export { AuthProvider, useAuthContext };
