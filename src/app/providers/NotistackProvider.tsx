import React, { useRef } from 'react';
import { SnackbarProvider, useSnackbar } from 'notistack';


// TODO переделать, сделать через хук
const NotistackProvider: React.FC = ({ children }) => {
	// add action to all snackbars
	const notistackRef = useRef<any>(null);
	const onClickDismiss = (key: any) => () => { 
		notistackRef.current.closeSnackbar(key);
	}

	return (
		<SnackbarProvider
			maxSnack={3} // максимальное количество снэкбаров в очереди
			// preventDuplicate={true} // предотвращает создание дублирующих уведомлений
			autoHideDuration={3000} // время жизни уведомлений
			anchorOrigin={{ // положение уведомлений на экране
				vertical: 'bottom',
				horizontal: 'center',
			}}
			// TransitionComponent={Grow} // эффект появления/исчезания уведомления
			ref={notistackRef} // ссылка на dom-элементы уведомлений
			// компонент как end-adornment для всех уведомлений
			// action={(key) => <Button onClick={onClickDismiss(key)}>ok</Button>}
			// либо полностью кастомный снэкбар
			// content={(key, message) => <MyCustomChildren id={key} message={message} />}
			classes={{ // классы для вариантов уведомлений
				// variantSuccess: classes.notistackSuccess,
				// variantError: classes.notistackError,
				// variantWarning: classes.notistackWarning,
				// variantInfo: classes.notistackInfo,
			}}
		>
			<SnackbarUtilsConfigurator />
			{children}
		</SnackbarProvider>
	);
}

let useSnackbarRef: any;
const SnackbarUtilsConfigurator = () => {
	useSnackbarRef = useSnackbar();
	return null;
} 

const notistack = {
	toast(message: string, variant: string = 'default') {
		useSnackbarRef.enqueueSnackbar(message, { variant });
	},

	success(message: string) {
		this.toast(message, 'success');
	},
	error(message: string) {
		this.toast(message, 'error');
	},
	warning(message: string) {
		this.toast(message, 'warning');
	},
	info(message: string) {
		this.toast(message, 'info');
	}
};

export { NotistackProvider, notistack };
