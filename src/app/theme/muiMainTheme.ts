import React from 'react';
import { createTheme, alpha } from '@mui/material/styles';
import { ruRU } from '@mui/material/locale';
import { // all of Material-design colors for castomization
	amber,
	blue,
	blueGrey,
	brown,
	common,
	cyan,
	deepOrange,
	deepPurple,
	green,
	grey,
	indigo,
	lightBlue,
	lightGreen,
	lime,
	orange,
	pink,
	purple,
	red,
	teal,
	yellow,
} from '@mui/material/colors';
import { ThemeContext } from '@emotion/react';


const muiMainTheme = createTheme({
	typography: {
		fontFamily: ['"Rubik"', '"Roboto"', '"Helvetica"', '"Arial"', 'sans-serif'].join(','),
	},

	components: {
		'MuiAppBar': {
			styleOverrides: {
				root: ({ theme }) => ({
					backgroundColor: alpha(theme.palette.primary.light, 0.2),
					borderTop: `1px solid ${alpha(theme.palette.primary.main, 0.2)}`,
					borderBottom: `1px solid ${alpha(theme.palette.primary.main, 0.2)}`,
					backdropFilter: 'blur(5px)',
				})
			}
		},
	},
}, ruRU);

export { muiMainTheme };
