import React, { useEffect, useRef } from 'react';
import { useTranslation } from 'react-i18next';

import {
	PageContainer,
	PageTitle,
	PageLoaderContainer,
} from 'app/components';
import { ProfileProvider } from './ProfileProvider';
import { ProfileCard } from './pages/ProfileCard';

import { useProfileCRUD } from 'hooks';


const ProfilePage: React.FC = () => {
	const { t } = useTranslation();

	const { isLoading, clearState } = useProfileCRUD();

	useEffect(() => {
		return clearState;
	}, []);


	return (
		<ProfileProvider>
			<PageContainer>
				<PageTitle title={t('profile.title')} subtitle={t('profile.subtitle')} />
				
				<PageLoaderContainer isLoading={isLoading}>
					<ProfileCard />
				</PageLoaderContainer>
			</PageContainer>
		</ProfileProvider>
	);
}

export default ProfilePage;
