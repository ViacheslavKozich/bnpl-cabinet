import { useCallback } from 'react';
import { useAppDispatch, useAppSelector, shallowEqual } from 'hooks';
import { profileSlice } from './profileSlice';
import * as profileThunkActions from './profileThunkActions';
// Types
import { IuseProfileCRUDReturn, IUser } from '../profileTypes';


const useProfileCRUD = (): IuseProfileCRUDReturn => {
	const dispatch = useAppDispatch();

	const profileState = useAppSelector((store) => store.profile, shallowEqual);
	const { userProfile, isLoading, lastError } = profileState;

	const fetchCurrentUserProfile = useCallback(async (): Promise<void> => {
		dispatch(profileThunkActions.getCurrentUserProfile());
	}, []);

	const changeCurrentUserProfile = useCallback((user: IUser): void => {
		dispatch(profileThunkActions.updateCurrentUserProfile(user));
	}, []);

	const clearState = useCallback((): void => {
		dispatch(profileSlice.actions.clearState());
	}, []);


	return {
		userProfile,
		isLoading,
		lastError,

		fetchCurrentUserProfile,
		changeCurrentUserProfile,
		clearState,
	};
}

export { useProfileCRUD };
