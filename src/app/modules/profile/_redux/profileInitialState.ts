// Types
import {
	IUser,
} from '../profileTypes';


export const initUser: IUser = {
	id:            '',
	firstName:     '',
	middleName:    '',
	lastName:      '',
	fullName:      '',
	roles:         [],
	photo:         '',
	email:         '',
	phone:         '',
	companyId:     '',
	isActive:      false,
	retailOutlets: null
};
