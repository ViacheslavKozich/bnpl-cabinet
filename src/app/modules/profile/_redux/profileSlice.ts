import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { initUser } from './profileInitialState';
// Types
import { IProfileState, IUser } from '../profileTypes';


const profileInitialState: IProfileState = {
	userProfile: initUser,
	isLoading:   false,
	lastError:   '',
};

const profileSlice = createSlice({
	name: 'profile',
	initialState: profileInitialState,
	reducers: {
		startCall: (state) => {
			state.isLoading = true;
			state.lastError = '';
		},
		catchError: (state, action: PayloadAction<string>) => {
			state.isLoading = false;
			state.lastError = action.payload;
		},
		clearState: (state) => {
			state.isLoading = false;
			state.lastError = '';
			console.log('clear profile state')
		},

		//* getCurrentUserProfile {ok: true}
		userProfileFetched: (state, action: PayloadAction<IUser>) => {
			state.isLoading = false;
			const fetchedProfile = action.payload;
			state.userProfile = fetchedProfile;
		},
		//* updateCurrentUserProfile {ok: true}
		userProfileUpdated: (state, action: PayloadAction<IUser>) => {
			state.isLoading = false;
			const updatedProfile = action.payload;
			state.userProfile = updatedProfile;
		}

	}
});

export { profileSlice };
