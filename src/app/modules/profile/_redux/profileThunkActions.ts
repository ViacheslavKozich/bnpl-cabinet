import { cabinetApiCalls } from 'apis/cabinetApi/cabinetApiCalls';
import { profileSlice } from './profileSlice';
import { notistack } from 'app/providers/NotistackProvider';
// Types
import { TThunkAction } from 'reduxStore/redux-store';
import { IUser } from '../profileTypes';


const profileActions = profileSlice.actions;

export const getCurrentUserProfile = (): TThunkAction<Promise<void>> => async (dispatch, getState) => {
	dispatch(profileActions.startCall());
	const response = await cabinetApiCalls.getCurrentUserProfile();
	if (response.ok && response.data) {
		dispatch(profileActions.userProfileFetched(response.data));
	} else {
		dispatch(profileActions.catchError(response.error));
	}
}

export const updateCurrentUserProfile = (user: IUser): TThunkAction<Promise<void>> => async (dispatch, getState) => {
	dispatch(profileActions.startCall());
	const response = await cabinetApiCalls.updateCurrentUserProfile(user);
	if (response.ok && response.data) {
		dispatch(profileActions.userProfileUpdated(response.data));
		notistack.success('Профиль пользователя успешно обновлен');
	} else {
		dispatch(profileActions.catchError(response.error));
	}
}
