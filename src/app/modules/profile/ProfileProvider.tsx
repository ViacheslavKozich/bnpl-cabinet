import React, { createContext, useContext, useState, useLayoutEffect, useRef, useMemo } from 'react';
import * as Yup from 'yup';
import { useTranslation } from 'react-i18next';
import { useProfileCRUD } from 'hooks';
// Types
import { FormikValues } from 'formik';
import { IProviderContext } from './profileTypes';


// @ts-ignores
const ProviderContext = createContext<IProviderContext>();
const useProfileContext = () => useContext(ProviderContext);


const ProfileProvider: React.FC = ({ children }) => {
	const { t } = useTranslation();
	const i18Prefix = 'profile.general-info';
	
	const { changeCurrentUserProfile } = useProfileCRUD();
	
	const profileFormSchema = useMemo(() => {
		return Yup.object().shape({
			firstName:  Yup.string().required(t('validation.required')),
			middleName: Yup.string(),
			lastName:   Yup.string(),
			email:      Yup.string().required(t('validation.required')).email(t('validation.email')),
			phone:      Yup.string().nullable(),
			photo:      Yup.string().nullable(),
		});
	}, []);

	const profileFormProps = useMemo(() => [
		{ title: t(`${i18Prefix}.first-name`),  name: 'firstName',  required: true  },
		{ title: t(`${i18Prefix}.middle-name`), name: 'middleName', required: false },
		{ title: t(`${i18Prefix}.last-name`),   name: 'lastName',   required: false },
		{ title: t(`${i18Prefix}.email`),       name: 'email',      required: true  },
		{ title: t(`${i18Prefix}.phone`),       name: 'phone',      required: false },
	], []);


	const contextValue = {
		profileFormSchema,
		profileFormProps,
		i18Prefix,
	};

	return (
		<ProviderContext.Provider value={contextValue}>
			{children}
		</ProviderContext.Provider>
	);
}

export { ProfileProvider, useProfileContext };
