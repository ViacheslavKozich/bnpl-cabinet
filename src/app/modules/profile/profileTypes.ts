// Types
import { AnyObjectSchema } from 'yup';
import {
	User
} from 'apis/cabinetApi/bnpl_cabinet_api/models';
import { FormikHandlers } from 'formik';


export interface IUser extends User {};

export interface IProviderContext {
	profileFormSchema: AnyObjectSchema;
	profileFormProps:  Array<{ title: string, name: string, required: boolean }>;
	i18Prefix:         string;
};

export interface IProfileState {
	/** Профиль текущего пользователя */
	userProfile: IUser;
	/** Статус обработки ассинхронного действия с профилем */
	isLoading:   boolean;
	/** Текст ошибки, пойманной при совершении запроса */
	lastError:   string;
};

/**
 * @property `userProfile` Профиль текущего пользователя
 * @property `isLoading` Статус обработки ассинхронного действия с профилем
 * @property `lastError` Текст ошибки, пойманной при совершении запроса
 * @property `fetchCurrentUserProfile` Получение профиля пользователя, если его нет в state
 * @property `changeCurrentUserProfile` Изменение профиля пользователя
 * @property `clearState` сброс isLoading и lastError при размонтировании модуля
 */
export interface IuseProfileCRUDReturn extends IProfileState {
	/** Получение профиля пользователя, если его нет в state */
	fetchCurrentUserProfile: () => Promise<void>;
	/** Изменение профиля пользователя */
	changeCurrentUserProfile: (user: IUser) => void;
	/** сброс isLoading и lastError при размонтировании модуля */
	clearState: () => void;
};
