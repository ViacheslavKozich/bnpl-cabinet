import React, { useMemo, useRef, useEffect, useCallback } from 'react';
import { Formik, Form, Field } from 'formik';
import { useTranslation } from 'react-i18next';
import { useProfileCRUD } from 'hooks';
import { useProfileContext } from '../ProfileProvider';

import {
	AppTextField,
	AppAvatar,
	Grid,
	ProfileDataItem,
	Typography,
} from 'app/components';


const EditProfileGeneralInfo: React.FC = () => {
	const { t } = useTranslation();

	const { userProfile } = useProfileCRUD();
	const profileContext = useProfileContext();
	const {
		profileFormSchema,
		profileFormProps,
		i18Prefix
	} = useMemo(() => profileContext, [profileContext]);

	return (
		<Formik
			// innerRef={}
			initialValues={userProfile}
			onSubmit={() => {}}
			validationSchema={profileFormSchema}
			enableReinitialize={true}
			validateOnMount
			validateOnChange
			validateOnBlur
		>
			{({ values, touched, errors, setFieldValue }) => (
				<Grid
					component={Form}
					container wrap="nowrap" justifyContent="space-between"
					spacing={2}
				>
					<Grid
						item flexGrow={1}
						container flexDirection="column" rowSpacing={2}
					>
						{profileFormProps.map(({ title, name, required }) => (
							<ProfileDataItem
								key={title}
								title={title}
								required={required}
								field={
									<AppTextField
										name={name}
										// @ts-ignore
										value={values[name]}
										setFieldValue={setFieldValue}
										errors={errors}
										touched={touched}
										// props...
										placeholder={title}
									/>
								}
							/>
						))}
					</Grid>

					<Grid
						item xs="auto"
						container flexDirection="column" alignItems="center"
						>
						<Typography sx={{ mb: 1 }}>{t(`${i18Prefix}.photo`)}</Typography>
						<AppAvatar src={values.photo} />
					</Grid>
				</Grid>
			)}
		</Formik>
	);
}


export { EditProfileGeneralInfo };

