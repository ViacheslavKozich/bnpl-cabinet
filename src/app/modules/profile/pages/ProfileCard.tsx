import React, { useEffect, useRef } from 'react';
import { useTranslation } from 'react-i18next';

import {
	EditableCard,
} from 'app/components';
import { ShowProfileGeneralInfo } from './ShowProfileGeneralInfo';
import { EditProfileGeneralInfo } from './EditProfileGeneralInfo';

import { useProfileContext } from '../ProfileProvider';
import { useProfileCRUD, useToggle } from 'hooks';


const ProfileCard: React.FC = () => {
	const { t } = useTranslation();

	const [isProfileEditing, setProfileEditing] = useToggle(false);

	return (
		<EditableCard
			title={t('profile.general-info.title')}
			isEditing={isProfileEditing}
			setEditing={setProfileEditing}
			submitForm={() => {}}
			saveDisabled={false}
		>
			{(isProfileEditing) ? (
				<EditProfileGeneralInfo/>
			) : (
				<ShowProfileGeneralInfo/>
			)}
		</EditableCard>
	);
}

export { ProfileCard };
