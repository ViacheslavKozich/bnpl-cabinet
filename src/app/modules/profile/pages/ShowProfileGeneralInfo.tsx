import React, { useMemo } from 'react';
import {
	AppAvatar,
	Grid,
	ProfileDataItem,
	Typography,
} from 'app/components';

import { useProfileCRUD } from 'hooks';
import { useTranslation } from 'react-i18next';


const ShowProfileGeneralInfo: React.FC = () => {
	const { t } = useTranslation();
	const prefix = 'profile.general-info';
	const { userProfile } = useProfileCRUD();

	const profileDataProps = useMemo(() => [
		{ title: 'full-name', value: userProfile.fullName },
		{ title: 'role',      value: userProfile.roles[0] },
		{ title: 'email',     value: userProfile.email    },
		{ title: 'phone',     value: userProfile.phone    },
	], [userProfile]);

	return (<>
		<Grid container wrap="nowrap" justifyContent="space-between" spacing={2}>
			<Grid
				item flexGrow={1}
				container flexDirection="column" rowSpacing={2}
			>
				{profileDataProps.map(({ title, value }) => (
					<ProfileDataItem
						key={title}
						title={t(`${prefix}.${title}`)}
						value={value}
					/>
				))}
			</Grid>

			<Grid
				item xs="auto"
				container flexDirection="column" alignItems="center"
			>
				<Typography sx={{ mb: 1 }}>{t(`${prefix}.photo`)}</Typography>
				<AppAvatar src={userProfile.photo} />
			</Grid>
		</Grid>
	</>);
}

export { ShowProfileGeneralInfo };
