import React from 'react'
import { notistack } from 'app/providers/NotistackProvider';


const HomePage: React.FC = () => {
	const showMessage = () => {
		notistack.success('сообщение об успехе');
	}

	return (
		<div>
			<h2>HomePage</h2>
			<button onClick={() => showMessage()}>show message</button>
		</div>
	);
}

export default HomePage;
