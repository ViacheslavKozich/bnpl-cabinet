import { cabinetApiCalls } from 'apis/cabinetApi/cabinetApiCalls';
import { tokensService } from 'app/auth/tokens/tokensService';
import { companySlice } from './companySlice';
import { notistack } from 'app/providers/NotistackProvider';
// Types
import { TThunkAction } from 'reduxStore/redux-store';
import { ICompanyProfile } from '../companyTypes';


const companyActions = companySlice.actions;

export const createCompany = (unp: string): TThunkAction<Promise<void>> => async (dispatch, getState) => {
	dispatch(companyActions.startCall());
	const response = await cabinetApiCalls.createCompany(unp);
	debugger
	if (response.ok && response.data) {
		dispatch(companyActions.companyCreated(response.data));
		// Получить новые токены, в клэймах которых будет статус админа
		await tokensService.updateTokensAfterAccessTokenExpEnd();
		notistack.success('Компания успешно зарегистрирована в приложении');
	} else {
		dispatch(companyActions.catchError(response.error));
	}
}

export const getCompanyById = (id: string): TThunkAction<Promise<void>> => async (dispatch, getState) => {
	dispatch(companyActions.startCall());
	const response = await cabinetApiCalls.getCompanyById(id);
	// debugger
	if (response.ok && response.data) {
		dispatch(companyActions.companyFetched(response.data));
	} else {
		dispatch(companyActions.catchError(response.error));
	}
}

export const updateCompany = (id: string, companyProfile: ICompanyProfile): TThunkAction<Promise<void>> => async (dispatch, getState) => {
	dispatch(companyActions.startCall());
	const response = await cabinetApiCalls.updateCompany(id, companyProfile);
	debugger
	if (response.ok && response.data) {
		dispatch(companyActions.companyUpdated(response.data));
		notistack.success('Профиль компании успешно обновлен');
	} else {
		dispatch(companyActions.catchError(response.error));
	}
}

export const markCompanyForDeletion = (id: string): TThunkAction<Promise<void>> => async (dispatch, getState) => {
	dispatch(companyActions.startCall());
	const response = await cabinetApiCalls.markCompanyForDeletion(id);
	debugger
	if (response.ok && response.data) {
		dispatch(companyActions.companyMarked());
		notistack.success('Компания помечена на удаление');
	} else {
		dispatch(companyActions.catchError(response.error));
	}
}

export const unmarkCompanyForDeletion = (id: string): TThunkAction<Promise<void>> => async (dispatch, getState) => {
	dispatch(companyActions.startCall());
	const response = await cabinetApiCalls.unmarkCompanyForDeletion(id);
	debugger
	if (response.ok && response.data) {
		dispatch(companyActions.companyMarked());
		notistack.success('Компания снята с удаления');
	} else {
		dispatch(companyActions.catchError(response.error));
	}
}
