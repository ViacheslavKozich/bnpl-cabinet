// Types
import {
	IAddress,
	IBankDetails,
	ICompanyProfile,
	ICurrency,
} from '../companyTypes';


export const initAdress: IAddress = {
	country: '',
	city:    '',
	street:  '',
	zip:     ''
};

export const initCurrency: ICurrency = {
	code:       '',
	alphaCode:  '',
	exponent:   '',
	isNational: false,
	isMain:     false,
	name:       '',
	isActive:   false
};

export const initBankDetails: IBankDetails = {
	bic:      '',
	code:     '',
	currency: initCurrency,
	iban:     '',
	bankName: '',
	address:  initAdress
};

export const initUserCompany: ICompanyProfile = {
	logo:           '',
	unp:            '',
	ownership:      '',
	name:           '',
	shortName:      '',
	address:        initAdress,
	bankDetails:    initBankDetails,
	contacts:       [],
	administrators: []
};
