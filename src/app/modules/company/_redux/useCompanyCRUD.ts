import { useCallback } from 'react';
import { useAppDispatch, useAppSelector, shallowEqual } from 'hooks';
import { companySlice } from './companySlice';
import * as companyThunkActions from './companyThunkActions';
// Types
import { IUseCompanyReturn, ICompanyProfile } from '../companyTypes';


const useCompanyCRUD = (): IUseCompanyReturn => {
	const dispatch = useAppDispatch();

	const companyState = useAppSelector((store) => store.company, shallowEqual);
	const { userCompany, isLoading, lastError } = companyState;

	const registerCompany = useCallback((unp: string): void => {
		dispatch(companyThunkActions.createCompany(unp));
	}, []);

	const fetchCompany = useCallback(async (id: string): Promise<void> => {
		dispatch(companyThunkActions.getCompanyById(id));
	}, []);

	const changeCompany = useCallback((id: string, company: ICompanyProfile): void => {
		dispatch(companyThunkActions.updateCompany(id, company));
	}, []);

	const markCompanyForDeletion = useCallback((id: string): void => {
		dispatch(companyThunkActions.markCompanyForDeletion(id));
	}, []);

	const unmarkCompanyForDeletion = useCallback((id: string): void => {
		dispatch(companyThunkActions.unmarkCompanyForDeletion(id));
	}, []);

	const clearCompanyState = useCallback((): void => {
		dispatch(companySlice.actions.clearState());
	}, []);


	return {
		userCompany,
		isLoading,
		lastError,
		registerCompany,
		fetchCompany,
		changeCompany,
		markCompanyForDeletion,
		unmarkCompanyForDeletion,
		clearCompanyState,
	};
}

export { useCompanyCRUD };
