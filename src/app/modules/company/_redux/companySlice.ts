import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { initUserCompany } from './companyInitialState';
// Types
import { ICompanyState, ICompanyProfile } from '../companyTypes';


const companyInitialState: ICompanyState = {
	userCompany: initUserCompany,
	isLoading:   false,
	lastError:   ''
};

const companySlice = createSlice({
	name: 'company',
	initialState: companyInitialState,
	reducers: {
		startCall: (state) => {
			state.isLoading = true;
			state.lastError = '';
		},
		catchError: (state, action: PayloadAction<string>) => {
			state.isLoading = false;
			state.lastError = action.payload;
		},
		clearState: (state) => {
			state.isLoading = false;
			state.lastError = '';
			console.log('clear company state')
		},

		//* createCompany {ok: true}
		companyCreated: (state, action: PayloadAction<ICompanyProfile>) => {
			state.isLoading = false;
			const createdCompany = action.payload;
			state.userCompany = createdCompany;
		},
		//* getCompanyById {ok: true}
		companyFetched: (state, action: PayloadAction<ICompanyProfile>) => {
			state.isLoading = false;
			const fetchedCompany = action.payload;
			state.userCompany = fetchedCompany;
		},
		//* updateCompany {ok: true}
		companyUpdated: (state, action: PayloadAction<ICompanyProfile>) => {
			state.isLoading = false;
			const updatedCompany = action.payload;
			state.userCompany = updatedCompany;
		},
		//* markCompanyForDeletion {ok: true} || unmarkCompanyForDeletion {ok: true}
		companyMarked: (state) => {
			state.isLoading = false;
		}

	}
});

export { companySlice };
