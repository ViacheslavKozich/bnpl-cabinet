// Types
import {
	Address,
	BankDetails,
	CompanyProfile,
	Currency,
} from 'apis/cabinetApi/bnpl_cabinet_api/models';


export interface IAddress extends Address {};
export interface IBankDetails extends BankDetails {};
export interface ICompanyProfile extends CompanyProfile {};
export interface ICurrency extends Currency {};

export interface ICompanyState {
	/** Компания, привязанная к текущему пользователю */
	userCompany: CompanyProfile;
	/** Статус обработки ассинхронного действия с компанией */
	isLoading:   boolean;
	/** Текст ошибки, пойманной при совершении запроса */
	lastError:   string;
};

/**
 * @property `userCompany` Компания, привязанная к текущему пользователю
 * @property `isLoading` Статус обработки ассинхронного действия с компанией
 * @property `lastError` Текст ошибки, пойманной при совершении запроса
 * @property `registerCompany` Создание бизнесс-аккаунта компании по унп
 * @property `fetchCompany` Получение профиля компании по id, если ее нет в state
 * @property `changeCompany` Изменение профиля компании
 * @property `markCompanyForDeletion` Добавление компании пометки на удаление
 * @property `unmarkCompanyForDeletion` Снятие с компании пометки на удаление
 * @property `clearCompanyState` сброс isLoading и lastError при размонтировании модуля
 */
export interface IUseCompanyReturn extends ICompanyState {
	/** Создание бизнесс-аккаунта компании по унп */
	registerCompany: (unp: string) => void;
	/** Получение профиля компании по id, если ее нет в state */
	fetchCompany: (id: string) => Promise<void>;
	/** Изменение профиля компании */
	changeCompany: (id: string, company: ICompanyProfile) => void;
	/** Добавление компании пометки на удаление */
	markCompanyForDeletion: (id: string) => void;
	/** Снятие с компании пометки на удаление */
	unmarkCompanyForDeletion: (id: string) => void;
	/** сброс isLoading и lastError при размонтировании модуля */
	clearCompanyState: () => void;
};
