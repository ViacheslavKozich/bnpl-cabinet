import React from 'react';
import { alpha } from '@mui/material/styles';
import {
	Avatar,
	Card,
	CardContent,
	CardHeader,
	Divider,
	Grid,
	Typography,
} from '@mui/material';
import { AppButton } from 'app/components';

import { useCompanyCRUD } from 'hooks';


const CompanyProfile: React.FC = () => {
	const { userCompany } = useCompanyCRUD();

	return (<>
		{/* Общая информация */}
		<Card sx={{ mb: 2 }}>
			<CardHeader
				title={"Общая информация компании"}
				action={<AppButton>редактировать</AppButton>}
				sx={{ background: (theme) => alpha(theme.palette.primary.light, 0.2) }}
			/>
			<CardContent>
				<Grid container wrap="nowrap" justifyContent="space-between" spacing={2}>
					<Grid
						item flexGrow={1}
						container flexDirection="column" rowSpacing={2}
					>
						{/* Компания */}
						<ProfileDataItem text="Компания" inputText={userCompany.name} />
						<Divider sx={{ transform: (theme) => `translateY(${theme.spacing(1)})` }} />

						{/* УНП */}
						<ProfileDataItem text="УНП" inputText={userCompany.unp} />
						<Divider sx={{ transform: (theme) => `translateY(${theme.spacing(1)})` }} />

						{/* Форма собственности */}
						<ProfileDataItem text="Форма собственности" inputText={userCompany.ownership} />
						<Divider sx={{ transform: (theme) => `translateY(${theme.spacing(1)})` }} />

						{/* Наименование организации */}
						<ProfileDataItem text="Наименование организации" inputText={userCompany.name} />
						<Divider sx={{ transform: (theme) => `translateY(${theme.spacing(1)})` }} />

						{/* Краткое наименование */}
						<ProfileDataItem text="Краткое наименование" inputText={userCompany.shortName} />
						<Divider sx={{ transform: (theme) => `translateY(${theme.spacing(1)})` }} />

						{/* Адрес организации */}
						<ProfileDataItem text="Адрес организации" inputText={userCompany.address.street} />
					</Grid>

					<Grid
						item xs="auto"
						container flexDirection="column" alignItems="center"
					>
						<Typography sx={{ mb: 1 }}>Логотип</Typography>
						<Avatar sx={{ width: 100, height: 100 }} src={userCompany.logo} />
					</Grid>
				</Grid>
			</CardContent>
		</Card>

		{/* Банковские реквизиты */}
		<Card sx={{ mb: 2 }}>
			<CardHeader
				title={"Банковские реквизиты"}
				action={<AppButton>редактировать</AppButton>}
				sx={{ background: (theme) => alpha(theme.palette.primary.light, 0.2) }}
			/>
			<CardContent>
				<Grid container wrap="nowrap" justifyContent="space-between" spacing={2}>
					<Grid
						item flexGrow={1}
						container flexDirection="column" rowSpacing={2}
					>
						{/* BIC */}
						<ProfileDataItem text="BIC" inputText={userCompany.bankDetails.bic} />
						<Divider sx={{ transform: (theme) => `translateY(${theme.spacing(1)})` }} />

						{/* Код банка */}
						<ProfileDataItem text="Код банка" inputText={userCompany.bankDetails.code} />
						<Divider sx={{ transform: (theme) => `translateY(${theme.spacing(1)})` }} />

						{/* Валюта */}
						<ProfileDataItem text="Валюта" inputText={userCompany.bankDetails.currency?.name} />
						<Divider sx={{ transform: (theme) => `translateY(${theme.spacing(1)})` }} />

						{/* IBAN */}
						<ProfileDataItem text="IBAN" inputText={userCompany.bankDetails.iban} />
						<Divider sx={{ transform: (theme) => `translateY(${theme.spacing(1)})` }} />

						{/* Наименование банка */}
						<ProfileDataItem text="Наименование банка" inputText={userCompany.bankDetails.bankName} />
						<Divider sx={{ transform: (theme) => `translateY(${theme.spacing(1)})` }} />

						{/* Адрес банка */}
						<ProfileDataItem text="Адрес банка" inputText={userCompany?.bankDetails.adress} />
					</Grid>
				</Grid>
			</CardContent>
		</Card>

		{/* Контактная информация */}
		<Card>
			<CardHeader
				title={"Контактная информация"}
				action={<AppButton>добавить контакт</AppButton>}
				sx={{ background: (theme) => alpha(theme.palette.primary.light, 0.2) }}
			/>
		</Card>
	</>);
}

interface IProfileDataItemProps {
	text:      string;
	inputText: string;
};
const ProfileDataItem: React.FC<IProfileDataItemProps> = ({ text, inputText }) => {
	return (
		<Grid
			item
			container wrap="nowrap" justifyContent="space-between" alignItems="center" spacing={2}>
			<Grid item minWidth={200}>
				<Typography color="text.secondary">{text}</Typography>
			</Grid>

			<Grid item flexGrow={1}>
				<Typography>{inputText}</Typography>
			</Grid>
		</Grid>
	);
}

export { CompanyProfile };
