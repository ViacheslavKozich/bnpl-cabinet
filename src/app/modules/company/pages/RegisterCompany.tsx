import React, { useState } from 'react';
import { alpha } from '@mui/material/styles';
import {
	Avatar,
	Card,
	CardActions,
	CardContent,
	CardHeader,
	Divider,
	Grid,
	TextField,
	Typography,
} from '@mui/material';
import { AppButton } from 'app/components';

import { useCompanyCRUD } from 'hooks';


const RegisterCompany: React.FC = () => {
	const [unp, setUnp] = useState<string>('');
	const { registerCompany } = useCompanyCRUD();

	return (<>
		<Card>
			<CardHeader
				title={"Регистрация компании в сервисе BNPL"}
				sx={{ background: (theme) => alpha(theme.palette.primary.light, 0.2) }}
			/>

			<CardContent>
				<Grid container wrap="nowrap" justifyContent="space-between" spacing={2}>
					<Grid
						item flexGrow={1}
						container flexDirection="column" rowSpacing={2}
					>
						{/* UNP */}
						<ProfileDataItem
							text="УНП компании"
							value={unp}
							onChange={(event: any) => setUnp(event.target.value)}
						/>
					</Grid>
				</Grid>
			</CardContent>

			<CardActions sx={{ background: (theme) => alpha(theme.palette.primary.light, 0.2), justifyContent: "flex-end" }}>
				{/* <AppButton onClick={() => cancelHandler()}>Отмена</AppButton> */}
				<AppButton variant="outlined" onClick={() => registerCompany(unp)}>Зарегистрировать</AppButton>
			</CardActions>
		</Card>
	</>);
}

interface IProfileDataItemProps {
	text:     string;
	value:    string;
	onChange: any;
};
const ProfileDataItem: React.FC<IProfileDataItemProps> = ({ text, value, onChange }) => {
	return (
		<Grid
			item
			container wrap="nowrap" justifyContent="space-between" alignItems="center" spacing={2}
		>
			<Grid item minWidth={200}>
				<Typography color="text.secondary">{text}</Typography>
			</Grid>

			<Grid item flexGrow={1}>
				<TextField value={value} size="small" onChange={onChange} fullWidth/>
			</Grid>
		</Grid>
	);
}

export { RegisterCompany };
