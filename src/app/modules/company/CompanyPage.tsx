import React, { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import {
	Box,
} from '@mui/material';

import {
	AppButton,
	PageContainer,
	PageTitle,
	PageLoaderContainer,
} from 'app/components';
import { notistack } from 'app/providers/NotistackProvider';
import { useAuth, useProfileCRUD, useCompanyCRUD } from 'hooks';

import { CompanyProfile } from './pages/CompanyProfile';
import { RegisterCompany } from './pages/RegisterCompany';


const CompanyPage: React.FC = () => {
	const { t } = useTranslation();
	const { isAdmin } = useAuth();
	const { isLoading, lastError, clearCompanyState } = useCompanyCRUD();

	useEffect(() => {
		if (lastError) notistack.error(lastError);
	}, [lastError]);

	useEffect(() => {
		return clearCompanyState;
	}, []);

	return (
		<PageContainer>
			<PageTitle title={t('company.title')} subtitle={t('company.subtitle')} />

			<PageLoaderContainer isLoading={isLoading}>
				{(isAdmin) ? (
					<CompanyProfile/>
				) : (
					<RegisterCompany/>
				)}
			</PageLoaderContainer>
		</PageContainer>
	)
}

export default CompanyPage;
